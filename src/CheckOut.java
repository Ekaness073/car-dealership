import java.util.Scanner;

public class CheckOut extends Main{

    public static String userInput;

    public void main(String[] args){
        this.userInput = userInput;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter 'list' for products list. Enter product name for specific information. Ex 'jeep' or 'fender. Be sure to use all lower case");

        userInput = scan.nextLine();
        if (userInput == "list"){
            System.out.println(Main.brand(P1));
            System.out.println(Main.brand(P2));
            System.out.println(Main.brand(P3));
            System.out.println(Main.brand(P4));
            System.out.println(Main.brand(P5));
        }
        if (userInput == "jeep"){
            System.out.println(Main.brand(P1));
            System.out.println(Main.type(P1));
            System.out.println(Main.price(P1));
            System.out.println(Main.speed(P1));
            System.out.println(Main.color(P1));
        }
        if (userInput == "toyota"){
            System.out.println(Main.brand(P2));
            System.out.println(Main.type(P2));
            System.out.println(Main.price(P2));
            System.out.println(Main.speed(P2));
            System.out.println(Main.color(P2));
        }
        if (userInput == "lexus"){
            System.out.println(Main.brand(P3) + ":");
            System.out.println(Main.type(P3));
            System.out.println(Main.price(P3));
            System.out.println(Main.speed(P3));
            System.out.println(Main.color(P3));

        }
        if (userInput == "fender"){
            System.out.println(Main.brand(P4));
            System.out.println(Main.price(P4));
        }
        if (userInput == "headlight"){
            System.out.println(Main.brand(P5));
            System.out.println(Main.price(P5));
        }
        if (userInput == "spark plug"){
            System.out.println(Main.brand(P6));
            System.out.println(Main.price(P6));
        }
    }
    public CheckOut(String P1,String P2, String P3, String P4, String P5, String P6) {
        super(P1, P2, P3, P4, P5, P6);
    }

    @Override
    public void color(String color) {
        super.color(color);
        P3 = "White";
        Return color;
    }
}
